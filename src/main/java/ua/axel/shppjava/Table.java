package ua.axel.shppjava;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.MissingFormatArgumentException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Table {
	private static final Logger LOGGER = LoggerFactory.getLogger(Table.class);
	public static final String TOO_BIG_MULTIPLY_RES_MESSAGE = "Max value is too big to get result of multiplying in this type";
	private static final int ROUND_DIGITS = 2;

	private final List<List<? extends Number>> values;

	public Table(String first, String last, String step, String type) {
		this.values = createTable(first, last, step, type);
	}

	private List<List<? extends Number>> createTable(String first, String last, String step, String type) {
		long lastLong;
		try {
			switch (type) {
				case "byte":
					lastLong = Byte.parseByte(last);
					if (lastLong * lastLong > Byte.MAX_VALUE) {
						throw new IllegalArgumentException(TOO_BIG_MULTIPLY_RES_MESSAGE);
					}
					return createTableLong(Byte.parseByte(first), lastLong, Byte.parseByte(step));
				case "short":
					lastLong = Short.parseShort(last);
					if (lastLong * lastLong > Short.MAX_VALUE) {
						throw new IllegalArgumentException(TOO_BIG_MULTIPLY_RES_MESSAGE);
					}
					return createTableLong(Short.parseShort(first), lastLong, Short.parseShort(step));
				case "int":
					lastLong = Integer.parseInt(last);
					if (lastLong * lastLong > Integer.MAX_VALUE) {
						throw new IllegalArgumentException(TOO_BIG_MULTIPLY_RES_MESSAGE);
					}
					return createTableLong(Integer.parseInt(first), lastLong, Integer.parseInt(step));
				case "long":
					lastLong = Long.parseLong(last);
					if (BigInteger.valueOf(lastLong)
							.multiply(BigInteger.valueOf(lastLong))
							.compareTo(BigInteger.valueOf(Long.MAX_VALUE)) > 0) {
						throw new IllegalArgumentException(TOO_BIG_MULTIPLY_RES_MESSAGE);
					}
					return createTableLong(Long.parseLong(first), lastLong, Long.parseLong(step));
				case "float":
					return createTableDouble(Float.parseFloat(first), Float.parseFloat(last), Float.parseFloat(step));
				case "double":
					return createTableDouble(Double.parseDouble(first), Double.parseDouble(last), Float.parseFloat(step));
				default:
					throw new MissingFormatArgumentException("There is no such type: " + type);
			}
		} catch (NumberFormatException e) {
			LOGGER.error("Wrong input. Some number is not {} type.", type, e);
		} catch (MissingFormatArgumentException e) {
			LOGGER.error("Wrong type: {}", type, e);
		} catch (IllegalArgumentException e) {
			LOGGER.error("Wrong input.", e);
		}
		return new ArrayList<>();
	}

	private static List<List<? extends Number>> createTableLong(long first, long last, long step) {
		return Stream.iterate(first, x -> x <= last, x -> x + step)
				.map(x -> Stream.iterate(first, y -> y <= last, y -> y + step)
						.map(y -> y * x)
						.collect(Collectors.toList()))
				.collect(Collectors.toList());
	}

	private static List<List<? extends Number>> createTableDouble(double first, double last, double step) {
		return Stream.iterate(first, x -> roundDouble(x) <= roundDouble(last), x -> x + step)
				.map(x -> Stream.iterate(first, y -> roundDouble(y) <= roundDouble(last), y -> y + step)
						.map(y -> roundDouble(y * x))
						.collect(Collectors.toList()))
				.collect(Collectors.toList());
	}

	@Override
	public String toString() {
		return values.stream()
				.map(l -> l.stream()
						.map(String::valueOf)
						.collect(Collectors.joining("\t")))
				.collect(Collectors.joining("\n"));
	}

	private static Double roundDouble(double d) {
		double helper = Math.pow(10, ROUND_DIGITS);
		return (int) Math.round(d * helper) / helper;
	}

}
