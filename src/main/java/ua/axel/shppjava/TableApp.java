package ua.axel.shppjava;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class TableApp {
	private static final Logger LOGGER = LoggerFactory.getLogger(TableApp.class);
	private static final String PROPERTIES_FILE = "properties.properties";
	private static final String PROPERTY_VALUE_MESSAGE = "Property: {}={}";
	private static final String SYS_PROPERTY_VALUE_MESSAGE = "System property: {}={}";

	private static final String FIRST_PROP_NAME = "first";
	private static final String LAST_PROP_NAME = "last";
	private static final String STEP_PROP_NAME = "step";
	private static final String TYPE_PROP_NAME = "type";
	private static final String DEFAULT_TYPE_VALUE = "int";

	public static void main(String[] args) {
		LOGGER.info("App started");
		var properties = getProperties();
		String firstValue = properties.getProperty(FIRST_PROP_NAME);
		LOGGER.debug(PROPERTY_VALUE_MESSAGE, FIRST_PROP_NAME, firstValue);
		String lastValue = properties.getProperty(LAST_PROP_NAME);
		LOGGER.debug(PROPERTY_VALUE_MESSAGE, LAST_PROP_NAME, lastValue);
		String stepValue = properties.getProperty(STEP_PROP_NAME);
		LOGGER.debug(PROPERTY_VALUE_MESSAGE, STEP_PROP_NAME, stepValue);
		String typeValue = getSystemProperty(TYPE_PROP_NAME);
		if (typeValue == null) {
			typeValue = DEFAULT_TYPE_VALUE;
			LOGGER.warn("Using default value for '{}': {}", TYPE_PROP_NAME, typeValue);
		}
		var table = new Table(firstValue, lastValue, stepValue, typeValue);
		LOGGER.info("{}", table);
		LOGGER.info("App finished");
	}

	private static String getSystemProperty(String propName) {
		String propValue = System.getProperty(propName);
		if (propValue == null) {
			LOGGER.warn("System property '{}' is absent.", propName);
			return null;
		}
		LOGGER.debug(SYS_PROPERTY_VALUE_MESSAGE, TYPE_PROP_NAME, propName);
		return propValue;
	}

	private static Properties getProperties() {
		var properties = new Properties();

		InputStream systemResourceAsStream = ClassLoader.getSystemResourceAsStream(PROPERTIES_FILE);
		if (systemResourceAsStream != null) {
			try {
				LOGGER.trace("Reading inner properties: {}", PROPERTIES_FILE);
				properties.load(new InputStreamReader(
						Objects.requireNonNull(systemResourceAsStream),
						StandardCharsets.UTF_8));
				LOGGER.info("Reading inner properties -- OK");
			} catch (IOException e) {
				LOGGER.error("Failed to read inner properties {}.", PROPERTIES_FILE, e);
			}
		} else {
			LOGGER.warn("There is not inner properties: {}", PROPERTIES_FILE);
		}

		var outsidePropertiesFile = new File(PROPERTIES_FILE);
		if (outsidePropertiesFile.exists() && !outsidePropertiesFile.isDirectory()) {
			try (var inputStreamReader = new InputStreamReader(new FileInputStream(PROPERTIES_FILE), StandardCharsets.UTF_8)) {
				LOGGER.trace("Reading outside properties: {}", PROPERTIES_FILE);
				properties.load(inputStreamReader);
				LOGGER.info("Reading outside properties -- OK");
			} catch (IOException e) {
				LOGGER.error("Failed to read file {}.", PROPERTIES_FILE, e);
			}
		} else {
			LOGGER.warn("There is not outside properties: {}", PROPERTIES_FILE);
		}
		return properties;
	}

}
