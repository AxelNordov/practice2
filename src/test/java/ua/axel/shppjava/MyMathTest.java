package ua.axel.shppjava;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class MyMathTest {

	@Test
	@DisplayName("Multiplying int")
	void testMultiplyInt() {
		assertEquals(0, MyMath.multiply(0, 5), "0 * 5 should equal 0");
		assertEquals(25, MyMath.multiply(5, 5), "5 * 5 should equal 25");
		assertEquals(-100, MyMath.multiply(10, -10), "10 * -10 should equal -100");
	}

	@ParameterizedTest(name = "{0} + {1} = {2}")
	@DisplayName("Dividing int")
	@CsvSource({
			" 0,    5,   0",
			" 5,    5,   1",
			"10,  -10,  -1",
			" 5,    2,   2"
	})
	void testDivideInt(int first, int second, int expectedResult) {
		assertEquals(expectedResult, MyMath.divide(first, second),
				() -> first + " + " + second + " should equal " + expectedResult);
	}

	@Test
	@DisplayName("Dividing int by zero")
	void testDivideIntByZero() {
		Exception exception = assertThrows(
				ArithmeticException.class, () -> MyMath.divide(5, 0));
		String expectedMessage = "/ by zero";
		String actualMessage = exception.getMessage();
		assertEquals(actualMessage, expectedMessage);
	}

	@Test
	@DisplayName("Multiplying double")
	void testMultiplyDouble() {
		assertEquals(0.0, MyMath.multiply(0.0, 5.0), 0.001);
		assertEquals(25.0, MyMath.multiply(5.0, 5.0), 0.001);
		assertEquals(25.0, MyMath.multiply(5.0, 5), 0.001);
		assertEquals(25.0, MyMath.multiply(5, 5.0), 0.001);
		assertEquals(-100.0, MyMath.multiply(10, -10), 0.001);
		assertEquals(15.3, MyMath.multiply(10.2, 1.5), 0.001);
	}

	@Test
	@DisplayName("Dividing double")
	void testDivideDouble() {
		assertEquals(0.0, MyMath.divide(0.0, 5.5), 0.001);
		assertEquals(1.0, MyMath.divide(5.7, 5.7), 0.001);
		assertEquals(1.25, MyMath.divide(5.0, 4), 0.001);
		assertEquals(12.5, MyMath.divide(5.0, 0.4), 0.001);
		assertEquals(-1.0, MyMath.divide(10.0, -10.0), 0.001);
		assertEquals(Double.POSITIVE_INFINITY, MyMath.divide(10.0, 0), 0.001);
		assertEquals(Double.POSITIVE_INFINITY, MyMath.divide(10.0, -0), 0.001);
		assertEquals(Double.NEGATIVE_INFINITY, MyMath.divide(10.0, -0.0), 0.001);
	}

}
