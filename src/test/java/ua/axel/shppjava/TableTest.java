package ua.axel.shppjava;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TableTest {

	@Test
	void createIntRegularTableForThree() {
		Table regularIntTableForThree = new Table("1", "3", "1", "int");
		assertEquals("1\t2\t3\n2\t4\t6\n3\t6\t9", regularIntTableForThree.toString());
	}

	@Test
	void createIntTableWithTooBigValue() {
		Table regularIntTableForThree = new Table(Integer.MAX_VALUE + "0", "3", "1", "int");
		assertEquals("1\t2\t3\n2\t4\t6\n3\t6\t9", regularIntTableForThree.toString());
		regularIntTableForThree = new Table("1", Integer.MAX_VALUE + "0", "1", "int");
		assertEquals("1\t2\t3\n2\t4\t6\n3\t6\t9", regularIntTableForThree.toString());
		regularIntTableForThree = new Table("1", "3", Integer.MAX_VALUE + "0", "int");
		assertEquals("1\t2\t3\n2\t4\t6\n3\t6\t9", regularIntTableForThree.toString());
	}

	@Test
	void createIntTableWithTooBigValueForMultiplying() {
		Table regularIntTableForThree = new Table("1", Integer.toString(Integer.MAX_VALUE), "1", "int");
		assertEquals("1\t2\t3\n2\t4\t6\n3\t6\t9", regularIntTableForThree.toString());
	}

	@Test
	void createIntTableWithNegatives() {
		Table regularIntTableForThree = new Table("-1", "1", "1", "int");
		assertEquals("1\t0\t-1\n0\t0\t0\n-1\t0\t1", regularIntTableForThree.toString());
	}

	@Test
	void createByteRegularTableForThree() {
		Table regularByteTableForThree = new Table("1", "3", "1", "byte");
		assertEquals("1\t2\t3\n2\t4\t6\n3\t6\t9", regularByteTableForThree.toString());
	}

	@Test
	void createByteTableWithTooBigValue() {
		Table regularByteTableForThree = new Table(Byte.MAX_VALUE + "0", "3", "1", "byte");
		assertEquals("1\t2\t3\n2\t4\t6\n3\t6\t9", regularByteTableForThree.toString());
		regularByteTableForThree = new Table("1", Byte.MAX_VALUE + "0", "1", "byte");
		assertEquals("1\t2\t3\n2\t4\t6\n3\t6\t9", regularByteTableForThree.toString());
		regularByteTableForThree = new Table("1", "3", Byte.MAX_VALUE + "0", "byte");
		assertEquals("1\t2\t3\n2\t4\t6\n3\t6\t9", regularByteTableForThree.toString());
	}

	@Test
	void createByteTableWithTooBigValueForMultiplying() {
		Table regularByteTableForThree = new Table("1", Byte.toString(Byte.MAX_VALUE), "1", "byte");
		assertEquals("1\t2\t3\n2\t4\t6\n3\t6\t9", regularByteTableForThree.toString());
	}

	@Test
	void createShortRegularTableForThree() {
		Table regularShortTableForThree = new Table("1", "3", "1", "short");
		assertEquals("1\t2\t3\n2\t4\t6\n3\t6\t9", regularShortTableForThree.toString());
	}

	@Test
	void createShortTableWithTooBigValue() {
		Table regularShortTableForThree = new Table(Short.MAX_VALUE + "0", "3", "1", "short");
		assertEquals("1\t2\t3\n2\t4\t6\n3\t6\t9", regularShortTableForThree.toString());
	}

	@Test
	void createShortTableWithTooBigValueForMultiplying() {
		Table regularByteTableForThree = new Table("1", Short.toString(Short.MAX_VALUE), "1", "short");
		assertEquals("1\t2\t3\n2\t4\t6\n3\t6\t9", regularByteTableForThree.toString());
	}

	@Test
	void createLongRegularTableForThree() {
		Table regularLongTableForThree = new Table("1", "3", "1", "long");
		assertEquals("1\t2\t3\n2\t4\t6\n3\t6\t9", regularLongTableForThree.toString());
	}

	@Test
	void createLongTableWithTooBigValue() {
		Table regularLongTableForThree = new Table(Long.MAX_VALUE + "0", "3", "1", "long");
		assertEquals("1\t2\t3\n2\t4\t6\n3\t6\t9", regularLongTableForThree.toString());
	}

	@Test
	void createLongTableWithTooBigValueForMultiplying() {
		Table regularByteTableForThree = new Table("1", Long.toString(Long.MAX_VALUE), "1", "long");
		assertEquals("1\t2\t3\n2\t4\t6\n3\t6\t9", regularByteTableForThree.toString());
	}

	@Test
	void createDoubleRegularTableForThree() {
		Table regularDoubleTableForThree = new Table("1d", "3.0", "1.", "double");
		assertEquals("1.0\t2.0\t3.0\n2.0\t4.0\t6.0\n3.0\t6.0\t9.0", regularDoubleTableForThree.toString());
	}

	@Test
	void createDoubleTableWithDouble() {
		Table regularDoubleTableForThree = new Table("1.1", "3.3000", "1.1D", "double");
		assertEquals("1.21\t2.42\t3.63\n" +
				"2.42\t4.84\t7.26\n" +
				"3.63\t7.26\t10.89", regularDoubleTableForThree.toString());
	}

	@Test
	void createFloatRegularTableForThree() {
		Table regularFloatTableForThree = new Table("1f", "3.0", "1.", "float");
		assertEquals("1.0\t2.0\t3.0\n2.0\t4.0\t6.0\n3.0\t6.0\t9.0", regularFloatTableForThree.toString());
	}

	@Test
	void createTableWithEmptyType() {
		Table regularIntTableForThree = new Table("1", "3", "1", "");
		assertEquals("1\t2\t3\n2\t4\t6\n3\t6\t9", regularIntTableForThree.toString());
	}

	@Test
	void createTableWithNullType() {
		Table regularIntTableForThree = new Table("1", "3", "1", null);
		assertEquals("1\t2\t3\n2\t4\t6\n3\t6\t9", regularIntTableForThree.toString());
	}

	@Test
	void createTableWithNullValues() {
		Table regularIntTableForThree = new Table(null, "3", "1", "int");
		assertEquals("1\t2\t3\n2\t4\t6\n3\t6\t9", regularIntTableForThree.toString());
	}

}